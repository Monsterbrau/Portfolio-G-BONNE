import { ajax } from "jquery";


export class Login{
  constructor() {
    this.apiUrl = 'https://www.simplonlyon.fr/promo6/gbonne/api/api/login_check';
  }
  getToken() {
    return sessionStorage.getItem('token');
  };

  login(username, password) {
    return ajax(this.apiUrl, {
      contentType: 'application/json',
      data: JSON.stringify({ username: username, password: password }),
      method: 'POST'
    })
      .then(resp => sessionStorage.setItem('token', resp.token));
  }

  logout() {
    sessionStorage.removeItem('token');
  };

}
