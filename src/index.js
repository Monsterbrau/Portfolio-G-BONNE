import { HomeController } from "./controller/homeController";
import * as $ from 'jquery';

let controller = new HomeController();

let url = 'https://www.simplonlyon.fr/promo6/gbonne/api';

let token = sessionStorage.getItem('token');

if (token) {
    $('#buttonLogout').show();
    $('#buttonAdmin').show();
    $('#buttonLogin').hide();


    $.ajax(url + '/project',
        {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        })
        .then(resp => {

            resp = JSON.parse(resp);
            console.log(resp);

        }).catch(err => {
            if (err) {
                $('#buttonAdmin').hide();
                $('#buttonLogout').hide();
                $('#lbuttonLogin').show();
            }
        });

} else {
    $('#buttonAdmin').hide();
    $('#buttonLogout').hide();
    $('#lbuttonLogin').show();
}

$('#loginModal').on('submit', event => {
    event.preventDefault();

    $.ajax(url + '/login_check',
        {
            method: 'POST',
            data: JSON.stringify({
                username: $('#username').val(),
                password: $('#password').val()
            }),
            contentType: 'application/json'
        })
        .then(resp => {
            sessionStorage.setItem('token', resp.token);
            window.location.reload();
            $('#buttonLogout').show();
            $('#buttonAdmin').show();
            $('#buttonLogin').hide();
        })
        .catch(err => console.error(err));

});

$('#buttonLogout').on('click', () => {
    sessionStorage.removeItem('token');
    $('#buttonLogout').hide();
    $('#buttonAdmin').hide();
    $('#buttonLogin').show();
    window.location.reload();
    

});


