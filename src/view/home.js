"use strict";
import $ from "jquery";

export class HomeView {

    constructor(list = []) {

        $("#projectList").empty();
        $("#pop").empty();

        list.forEach(projectList => {

            let container = document.createElement("div");
            container.setAttribute("class", "card col-md-3");

            let imgBalise = document.createElement("img");
            imgBalise.setAttribute("class", "card-img-top");
            imgBalise.src = projectList.img;
            imgBalise.alt = projectList.name;

            $("#projectList").append(container);
            $(container).append(imgBalise);

            let cardBody = document.createElement('div');
            cardBody.setAttribute('class', 'card-body')

            let titleContent = document.createElement("h3");
            let description = document.createElement("p");
            titleContent.textContent = projectList.name;
            description.textContent = projectList.description;

            let divButton = document.createElement('div');
            divButton.setAttribute('class','forButton')

            let link = document.createElement("a");
            link.textContent = "Go sur projet";
            link.setAttribute('class', 'btn btn-primary but');
            link.href = projectList.link;

            let git = document.createElement("a");
            git.textContent = "Projet sur Git";
            git.setAttribute('class', 'btn btn-primary but');
            git.href = projectList.git;

            $(container).append(cardBody);
            $(cardBody).append(titleContent).append(description).append(divButton);
            $(divButton).append(link).append(git);

        });

        list.forEach(projectList => {

            let ContainerModal = document.createElement("div");
            ContainerModal.setAttribute("class", "card");

            let imgBaliseModal = document.createElement("img");
            imgBaliseModal.setAttribute("class", "card-img-top");
            imgBaliseModal.src = projectList.img;

            let deleteButtonModal = document.createElement("button");
            deleteButtonModal.setAttribute('class', 'btn btn-danger but');

            $(deleteButtonModal).click(() => {
                console.log("home");
                $(document).trigger('deleteProject', projectList);

            })

            deleteButtonModal.textContent = "Supprimer";

            let cardBodyModal = document.createElement('div');
            cardBodyModal.setAttribute('class', 'card-body')

            let titleContentModal = document.createElement("h3");
            let descriptionModal = document.createElement("p");
            titleContentModal.textContent = projectList.name;
            descriptionModal.textContent = projectList.description;

            let linkModal = document.createElement("a");
            linkModal.textContent = "Go sur projet";
            linkModal.setAttribute('class', 'btn btn-primary but');
            linkModal.href = projectList.link;

            let gitModal = document.createElement("a");
            gitModal.textContent = "Projet sur Git";
            gitModal.setAttribute('class', 'btn btn-primary but');
            gitModal.href = projectList.git;

            $('#pop').append(ContainerModal);

            $(ContainerModal).append(imgBaliseModal).append(cardBodyModal);

            $(cardBodyModal).append(titleContentModal).append(descriptionModal).append(linkModal).append(gitModal).append(deleteButtonModal);
        });

        $('#form').off('submit');
        $("#form").on('submit', function (event) {
            event.preventDefault();
            console.log("home");
            $(document).trigger('addProject', new FormData(this));
        });

        // $('#MAJ-form').off('submit');
        // $("#MAJ-form").on('submit', function(event)  {
        //     event.preventDefault();
        //     console.log('test')
        //     $(document).trigger('updateProject', new FormData(this));
        // });
    };
}

