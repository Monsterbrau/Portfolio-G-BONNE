"use strict";

import { ProjectRepository } from "../model/repository/projectRepository";
import { HomeView } from "../view/home";
import $ from "jquery";




export class HomeController {
    constructor() {
        let repo = new ProjectRepository();
        let promise = repo.findAll();

        $(document).on('deleteProject', (event, projectList) => {
            console.log("home controller");
            repo.delete(projectList.id).then(response => {
                repo.findAll().then(response => {
                    new HomeView(response);
                })
            });
        });

        $(document).on('addProject', (event, projectNew) => {
            console.log("home controller");
            repo.add(projectNew).then(response => {
                repo.findAll().then(response => {
                    new HomeView(response);
                })
            });
        });

        promise.then(response => {
            new HomeView(response);
        });
    }
}
