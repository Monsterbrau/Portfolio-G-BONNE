"strict"

export class Project {
  constructor(id, name, description, link, git, img) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.link = link;
    this.git = git;
    this.img = img;
  }
}