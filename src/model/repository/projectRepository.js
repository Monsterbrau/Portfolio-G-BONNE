"use strict";
import $ from 'jquery';
import { Project } from "../entity/project";
import { Login } from '../../service/login';

export class ProjectRepository {
    constructor() {
        this.baseUrl = "https://www.simplonlyon.fr/promo6/gbonne/api";
        this.loginService = new Login();
    }
    
    findAll() {
        let url = `${this.baseUrl}/project`;
        let promise = $.ajax(url).then(function (response) {
            let projectList = [];
            response = JSON.parse(response);

            for (const element of response) {
                let instance = new Project(element.id, element.name, element.description, element.link, element.git, element.img);
                projectList.push(instance);
            }

            return projectList;
        });

        promise.catch(error => {
            console.error(`${error.status} ${error.statusText} ${url}`);
        });

        return promise;
    };

    delete(id) {
        let url = this.baseUrl;
        let promise = $.ajax(`${url}/project/${id}`, {
            headers: {
                'Authorization': 'Bearer ' + this.loginService.getToken()
            }, method: "DELETE"
        }).catch(err => {
            if(err.status === 401) {
                this.loginService.logout();
            }
            throw err;
        });

        promise.catch(error => {
            console.error(`${error.status} ${error.statusText} ${url} ${id}`);
        });
        return promise;
    };

    add(projectNew) {
        let url = `${this.baseUrl}/project`;
        let promise = $.ajax(url, {
            headers: {
                'Authorization': 'Bearer ' + this.loginService.getToken()
            }, method: "POST", data: projectNew, contentType: false, processData: false
        }).catch(err => {
            if(err.status === 401) {
                this.loginService.logout();
            }
            throw err;
        });

        promise.catch(error => {
            console.error(`${error.status} ${error.statusText} ${url}`);
        });

        return promise;
    };

    // update(projectList) {
    //     let json = JSON.stringify(projectList);
    //     let url = this.baseUrl;
    //     let promise = $.ajax(`${url}project/${projectList.id}`, { method: "PUT", data: json });

    //     promise.catch(error => {
    //         console.error(`${error.status} ${error.statusText} ${url}`);
    //     });

    //     return promise;
    // }
}
